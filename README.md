# DataWarehouse

Este es el último proyecto del cuarto bloque de la carrera: Desarrollo Web FullStack.

### Pre-requisitos 📋

Conocimientos en Node.js con Express.

## Construido con 🛠️

Estas fueron las herramientas usadas para el desarrollo de este proyecto:

* [Node.js](https://nodejs.org/en/) 
* [ExpressJs](https://expressjs.com/)
* [JWT](https://www.npmjs.com/package/jsonwebtoken)
* [Sequelize](https://sequelize.org/)
* [Swagger](https://swagger.io/) - Documentacón API.
* [Bootstrap 5](https://getbootstrap.com/)
* [Sass](https://sass-lang.com/)

## Versionado 📌

La indea es implementar [SemVer](http://semver.org/) para el versionado, por lo pronto se asigna la v0.0.1

## Licencia 📄

Este proyecto está bajo la Licencia  MIT.

## Demo 🖥

* [DataWarehouse](https://davidmorenocode.co/projects/datawarehouse/public/).
* [Documentación API](https://app.swaggerhub.com/apis-docs/davidmorenocode/Datawarehouse/1.0.0).

## Colección Postman 🚀

* [Colección Datawarehouse](https://www.getpostman.com/collections/28d42b25fd2875f3cd33).

## Instalación 📔

* #### Proyecto:
  * Clonar el repositorio.
  * ejecutar `npm install`.
  * renombrar el archivo `.env.example` por `.env`
  * ejecutar `npm run dev`.

* #### Base de datos:
  * instalar xampp.
  * iniciar Mysql.
  * crear base de datos: `db_datawarehouse`.
  * Seleccionar la base de datos.
  * importar el `db_datawarehouse.sql` .

* #### Usuarios:
  * Administrador:
    * user: admin@admin.com 
    * pass: 123456
  * Básico:
    * user: basico@basico.com
    * pass: 123456

* #### Postman:
  * Importar la colección ([Colección Datawarehouse](https://www.getpostman.com/collections/28d42b25fd2875f3cd33)) - o la adjunta en el repositorio.

## Capturas 📷

<img src="public/assets/images/cap-1.png" width=800 />
<img src="public/assets/images/cap-2.png" width=800 />
<img src="public/assets/images/cap-3.png" width=800 />
<img src="public/assets/images/cap-4.png" width=800 />
<img src="public/assets/images/cap-5.png" width=800 />
<img src="public/assets/images/cap-6.png" width=800 />


----
⌨️ por [David Moreno](https://www.linkedin.com/in/davidmorenocode/) 🤓☕
