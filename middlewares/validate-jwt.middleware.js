const jwt =  require('jsonwebtoken');

const validateJWT = (req, res, next) =>{
  const token = req.header('Authorization');

  if(!token){
    return res.json("No se envio el token en la petición").status(401);
  }

  try {
    const  {uid, roleId} = jwt.verify(token, process.env.JWT_SECRET);
    req.uid = uid;
    req.roleId = roleId;
    next();
  } catch (error) {
    console.log(error);
    res.json("Token no válido").status(401);
  }
}

module.exports = {
  validateJWT
}