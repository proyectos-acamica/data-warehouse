const isAdmin = (req, res, next) =>{
  const {roleId} = req

  if(roleId === 1){
    next();
  }else{
    res.json('Este rol no tiene permisos, solo el rol Administrador, puede ejercer esta acción.');
  }
}


module.exports = {
  isAdmin
}