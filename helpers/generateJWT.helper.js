const jwt = require('jsonwebtoken');

const {JWT_SECRET} = process.env;


class Token {

  getToken (uid = "", roleId =""){
    return new Promise((resolve, reject) =>{
      const payload = {uid, roleId};

      jwt.sign(payload,JWT_SECRET,{
        expiresIn: "4h"
      },(error, token) =>{
        if(error){
          console.log("Error con el token: ",error);
          reject("No se generó el token.");
        }else{
          resolve(token);
        }
      });
    })
  }

}


module.exports = new Token();