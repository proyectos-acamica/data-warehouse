class Utilities {

  formatFields(tablePrefix, action, data){
    let arrColumns = Object.keys(data);
    let arrValues =  Object.values(data)
    let newData = {};
    
    arrColumns.forEach((column, index) =>{
      newData[`${tablePrefix}_${column}`] = arrValues[index];
    } );

    if(action === 'insert' ){
      arrColumns = Object.keys(newData);
      arrValues = Object.values(newData).join("','");

      return  {arrColumns, arrValues}
    }else{
      arrColumns = Object.keys(newData);
      arrValues = Object.values(newData);

      const arrColumnsValues = arrColumns.map((column, index) => `${column} = "${arrValues[index]}"`).toString();

      return arrColumnsValues;
    }

  }

}

module.exports = new Utilities();