const API_URL =  'http://localhost:3000';

const isToken = () =>{
  const token = localStorage.getItem('token');
  if(token === null){
    return false;
  }else{
    return token;
  }
}

const authenticate = async (userData) =>{
  
  const endpoint = `${API_URL}/auth/login`;
  const data = await fetch(endpoint,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userData)
  })
  return await data.json();
}


//* Users

const getAllUsers = async () =>{
  const endpoint = `${API_URL}/users`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}

const createUser = async (userData) =>{
  const endpoint = `${API_URL}/users`;
  const data = await fetch(endpoint,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(userData)
  })
  return await data.json();
}

const editUser = async (user_id, userData) =>{
  const endpoint = `${API_URL}/users/${user_id}`;
  const data = await fetch(endpoint,{
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(userData)
  })
  return await data.json();
}

const deleteUser  = async (user_id) =>{
  const endpoint = `${API_URL}/users/${user_id}`;
  const data = await fetch(endpoint,{
    method: 'DELETE',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}


//* Contacts


const getAllContacts  = async () =>{
  const endpoint = `${API_URL}/contacts`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}

const createContact = async (contactData) =>{
  const endpoint = `${API_URL}/contacts`;
  const data = await fetch(endpoint,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(contactData)
  })
  return await data.json();
}

const editContact = async (contact_id, contactData) =>{
  const endpoint = `${API_URL}/contacts/${contact_id}`;
  const data = await fetch(endpoint,{
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(contactData)
  })
  return await data.json();
}

const updateChannels = async (contact_id, contactData) =>{
  const endpoint = `${API_URL}/contacts/channels/contact/${contact_id}`;
  const data = await fetch(endpoint,{
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(contactData)
  })
  return await data.json();
}

const deleteContact  = async (contact_id) =>{
  const endpoint = `${API_URL}/contacts/${contact_id}`;
  const data = await fetch(endpoint,{
    method: 'DELETE',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}

const deleteContactMultiple  = async (contactData) =>{  
  const endpoint = `${API_URL}/contacts/`;
  const data = await fetch(endpoint,{
    method: 'DELETE',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }, 
    body: JSON.stringify(contactData)
  })

  return await data.json();
}

const createChannels = async (contactData) =>{
  const endpoint = `${API_URL}/contacts/channels`;
  const data = await fetch(endpoint,{
    method: 'POST',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }, 
    body: JSON.stringify(contactData)
  })

  return await data.json();
}

const getAllChannels = async () =>{
  const endpoint = `${API_URL}/contacts/channels`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })
  return await data.json();
}

const getAllRelationsContact = async (contact_id) =>{
  const endpoint = `${API_URL}/contacts/channels/contact/${contact_id}`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })
  return await data.json();
}

//* Regiones

const getAllRegions = async () =>{
  const endpoint = `${API_URL}/regions`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })
  return await data.json();
}

const createRegion = async (dataRegions) =>{
  const endpoint = `${API_URL}/regions`;
  const data = await fetch(endpoint,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(dataRegions)
  })
  return await data.json();
}

const editRegion = async (region_id, regionData) =>{
  const endpoint = `${API_URL}/regions/${region_id}`;
  const data = await fetch(endpoint,{
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(regionData)
  })
  return await data.json();
}

const removeRegion = async (region_id) =>{
  const endpoint = `${API_URL}/regions/${region_id}`;
  const data = await fetch(endpoint,{
    method: 'DELETE',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}


//* Countries

const getAllCountriesByRegion = async (region_id) =>{
  const endpoint = `${API_URL}/countries/region/${region_id}`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })
  return await data.json();
}

const createCountry = async (dataCountry) =>{
  const endpoint = `${API_URL}/countries`;
  const data = await fetch(endpoint,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(dataCountry)
  })
  return await data.json();
}

const editCountry = async (contry_id, countryData) =>{
  const endpoint = `${API_URL}/countries/${contry_id}`;
  const data = await fetch(endpoint,{
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(countryData)
  })
  return await data.json();
}

const removeCountry = async (country_id) =>{
  const endpoint = `${API_URL}/countries/${country_id}`;
  const data = await fetch(endpoint,{
    method: 'DELETE',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}


//* Cities

const getAllCities = async () => {
  const endpoint = `${API_URL}/cities`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}

const getAllCitiesByCountry = async (country_id) =>{  
  const endpoint = `${API_URL}/cities/country/${country_id}`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })
  return await data.json();
}

const createCity = async (dataCity) =>{
  const endpoint = `${API_URL}/cities`;
  const data = await fetch(endpoint,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(dataCity)
  })
  return await data.json();
}

const editCity = async (city_id, cityData) =>{
  const endpoint = `${API_URL}/cities/${city_id}`;
  const data = await fetch(endpoint,{
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(cityData)
  })
  return await data.json();
}

const removeCity = async (city_id) =>{
  const endpoint = `${API_URL}/cities/${city_id}`;
  const data = await fetch(endpoint,{
    method: 'DELETE',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}



//* Companies

const getAllCompanies = async () =>{
  const endpoint = `${API_URL}/companies`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}

const createCompany = async (companyData) =>{
  const endpoint = `${API_URL}/companies`;
  const data = await fetch(endpoint,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(companyData)
  })
  return await data.json();
}

const editCompany = async (company_id, companyData) =>{
  const endpoint = `${API_URL}/companies/${company_id}`;
  const data = await fetch(endpoint,{
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': isToken()
    },
    body: JSON.stringify(companyData)
  })
  return await data.json();
}

const deleteCompany  = async (company_id) =>{
  const endpoint = `${API_URL}/companies/${company_id}`;
  const data = await fetch(endpoint,{
    method: 'DELETE',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}

//* Extras

const getAllRoles = async ()=>{

  const endpoint = `${API_URL}/users/roles`;
  const data = await fetch(endpoint,{
    method: 'GET',
    headers:{
      'Content-Type': 'application/json',
      'Authorization': isToken()
    }
  })

  return await data.json();
}

export{
  authenticate,
  getAllUsers,
  createUser,
  editUser,
  deleteUser,
  getAllRoles,
  getAllRegions,
  createRegion,
  editRegion,
  removeRegion,  
  getAllCountriesByRegion,
  createCountry,
  editCountry,
  removeCountry,
  getAllCities,
  getAllCitiesByCountry,
  createCity,
  editCity,
  removeCity,
  getAllCompanies,
  createCompany,
  editCompany,
  deleteCompany,
  getAllContacts,
  createContact,
  editContact,
  deleteContact,
  deleteContactMultiple,
  getAllChannels,
  createChannels,
  getAllRelationsContact,
  updateChannels
}