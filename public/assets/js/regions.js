import {getAllRegions, createRegion, editRegion, removeRegion, getAllCountriesByRegion, createCountry, editCountry, removeCountry, getAllCitiesByCountry, createCity, editCity, removeCity}  from './requests.js'


const accordionRegions = document.querySelector('#accordionRegions');
const btnCreateRegion = document.querySelector('#btnCreateRegion');
const formEditRegion = document.querySelector('#formEditRegion');
const btnEditRegion = document.querySelector('#btnEditRegion');
const btnDeleteRegion = document.querySelector('#btnDeleteRegion');
const formEditCountry = document.querySelector('#formEditCountry');
const btnCreateCountry = document.querySelector('#btnCreateCountry');
const btnEditCountry = document.querySelector('#btnEditCountry');
const formEditCity = document.querySelector('#formEditCity');
const btnCreateCity = document.querySelector('#btnCreateCity');
const btnEditCity = document.querySelector('#btnEditCity');

const toastTitle = document.querySelector('#toastTitle');
const toastTime = document.querySelector('#toastTime');
const toastMessage = document.querySelector('#toastMessage');

const toast = document.querySelector("#liveToast");
const newAlert = new bootstrap.Toast(toast);

//* CRUD REGIONES

const getAllRegionsDB = async () =>{
  const data = await getAllRegions();
  console.log(data);
  createRegions(data)
}

const createRegions = (regionsData) =>{
  let arrRegionsId = []
  accordionRegions.innerHTML = '';
  regionsData.forEach((registry) =>{
    accordionRegions.innerHTML += `
    <div class="accordion-item">
    <h2 class="accordion-header" id="region_${registry.id}">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#region_body_${registry.id}" aria-expanded="true" aria-controls="region_body_${registry.id}">
        ${registry.name}
      </button>
      <div class="containerBtn">
        <i class="bi bi-pencil-square btnOpenModalEditRegion" data-bs-toggle="modal" data-bs-target="#editRegionModal" data-region-edit-id="${registry.id}" data-region-name="${registry.name}" ></i>  
        <i class="bi bi-trash btnOpenModalDeleteRegion" data-bs-toggle="modal" data-bs-target="#deleteRegionModal" data-region-delete-id="${registry.id}" ></i>
        <i class="bi bi-plus-circle btnOpenModalCreateRegion" data-bs-toggle="modal" data-bs-target="#createCountryModal" data-region-id="${registry.id}"></i>
      </div>
    </h2>
    <div id="region_body_${registry.id}" class="accordion-collapse collapse " aria-labelledby="region_${registry.id}" data-bs-parent="#accordionRegions">
      <div class="accordion-body" id="region_country_${registry.id}">
        <div class="accordion" id="accordionCountries_${registry.id}">
        </div>
      </div>
    </div>
  </div>
      `;
    arrRegionsId.push(registry.id);
  });

  const btnOpenModalEditRegion = document.querySelectorAll('.btnOpenModalEditRegion');
  const btnOpenModalDeleteRegion = document.querySelectorAll('.btnOpenModalDeleteRegion');
  const btnOpenModalCreateRegion = document.querySelectorAll('.btnOpenModalCreateRegion');

  btnOpenModalEditRegion.forEach((btn) =>{
    btn.addEventListener('click', createModalEditRegion);
  });
  btnOpenModalDeleteRegion.forEach((btn) =>{
    btn.addEventListener('click', setIdRegionDelete);
  });
  btnOpenModalCreateRegion.forEach((btn) =>{
    btn.addEventListener('click', setIdRegionCreateCountry);
  });

  getAllCountriesByRegionDB(arrRegionsId);
}

const createModalEditRegion = (event) =>{
  console.log(event)
  const  name = event.target.dataset.regionName;
  const  id = event.target.dataset.regionEditId;
  

  formEditRegion.innerHTML= `
    <label for="inputEditRegion" class="form-label">Región:</label>
    <input type="text" class="form-control inputsEditRegion" id="inputEditRegion" value="${name}" data-name-field="name">
  `;
  btnEditRegion.setAttribute('data-region-edit-id', id);
  btnEditRegion.setAttribute('data-region-edit-name', name);
}

const insertRegion = async () =>{
  const inputCreateRegion = document.querySelector('#inputCreateRegion').value;
  const returnData = await createRegion({name:inputCreateRegion});

  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Región Creada"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();
}

const updateRegion = async  (event) =>{
  const id = event.target.dataset.regionEditId;
  const name = document.querySelector('#inputEditRegion').value;
  const returnData = await editRegion(id, {name});
  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Region Actualizada"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();

}

const setIdRegionDelete = async (event) =>{
  const id = event.target.dataset.regionDeleteId;
  btnDeleteRegion.setAttribute('data-region-delete', id);
}

const deleteRegion = async (event) =>{
  console.log('Delete: ', event);
  const id = event.target.dataset.regionDelete;
  const returnData = await removeRegion(id);

  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Region Eliminada"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();
}

//* CRUD PAISES

const getAllCountriesByRegionDB = async (arrRegionsId) =>{
  arrRegionsId.forEach( async (element) =>{
    const data = await getAllCountriesByRegion(element);
    console.log(data);
    createCountries(data, element);
  });

}

const createCountries = (data, region_id) =>{
  const accordionCountry = document.querySelector(`#accordionCountries_${region_id}`);
  let arrCountriesId = [];
  data.forEach((registry) =>{
    accordionCountry.innerHTML += `
    <div class="accordion-item">
    <h2 class="accordion-header" id="country_${registry.id}">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#country_body_${registry.id}" aria-expanded="true" aria-controls="country_body_${registry.id}">
        ${registry.name}
      </button>
      <div class="containerBtn">
        <i class="bi bi-pencil-square btnOpenModalEditCountry" data-bs-toggle="modal" data-bs-target="#editCountryModal" data-country-edit-id="${registry.id}" data-country-name="${registry.name}" ></i>  
        <i class="bi bi-trash btnOpenModalDeleteCountry" data-bs-toggle="modal" data-bs-target="#deleteCountryModal" data-country-delete-id="${registry.id}" ></i>
        <i class="bi bi-plus-circle btnOpenModalCreateCountry" data-bs-toggle="modal" data-bs-target="#createCityModal" data-country-id ="${registry.id}"></i>
      </div>
    </h2>
    <div id="country_body_${registry.id}" class="accordion-collapse collapse " aria-labelledby="country_${registry.id}" data-bs-parent="#accordionCountries_${region_id}">
      <div class="accordion-body" id="country_city_${registry.id}">
        <table class="table table-striped table-hover" id="tableCities_${registry.id}">
          <thead class="table__head">
            <tr>
              <th class="table__th">Ciudad</th>
              <th class="table__th">Acciones</th>
            </tr>
          </thead>
          <tbody id ="bodyCities${registry.id}"></tbody>
        </table>
      </div>
    </div>
  </div>
      `;
      arrCountriesId.push(registry.id);
  });

  const btnOpenModalEditCountry = document.querySelectorAll('.btnOpenModalEditCountry');
  const btnOpenModalCreateCountry = document.querySelectorAll('.btnOpenModalCreateCountry');
  const btnOpenModalDeleteCountry = document.querySelectorAll('.btnOpenModalDeleteCountry');

  btnOpenModalEditCountry.forEach((btn) =>{
    btn.addEventListener('click', createModalEditCountry);
  });
  btnOpenModalDeleteCountry.forEach((btn) =>{
    btn.addEventListener('click', setIdCountryDelete);
  });
  btnOpenModalCreateCountry.forEach((btn) =>{
    btn.addEventListener('click', setIdCountryCreateCity);
  });

  getAllCitiesByCountryDB(arrCountriesId);
}

const insertCountry = async (event) =>{
  const region_id = event.target.dataset.regionId;
  const name = document.querySelector('#inputCreateCountry').value;
  
  const returnData = await createCountry({name, region_id});

  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "País Creado"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();
}

const setIdRegionCreateCountry = (event) =>{
  const id = event.target.dataset.regionId;
  btnCreateCountry.setAttribute('data-region-id', id);
}

const createModalEditCountry = (event) =>{
  console.log(event)
  const  name = event.target.dataset.countryName;
  const  id = event.target.dataset.countryEditId;
  

  formEditCountry.innerHTML= `
    <label for="inputEditCountry" class="form-label">País:</label>
    <input type="text" class="form-control inputsEditCountry" id="inputEditCountry" value="${name}" data-name-field="name">
  `;
  btnEditCountry.setAttribute('data-country-edit-id', id);
  btnEditCountry.setAttribute('data-country-edit-name', name);
}

const updateCountry = async (event) =>{
  const id = event.target.dataset.countryEditId;
  const name = document.querySelector('#inputEditCountry').value;
  const returnData = await editCountry(id, {name});
  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "País Actualizado"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();
}

const setIdCountryDelete = (event) =>{
  const id = event.target.dataset.countryDeleteId;
  btnDeleteCountry.setAttribute('data-country-delete', id);
}

const deleteCountry = async (event) =>{
  console.log('Delete: ', event);
  const id = event.target.dataset.countryDelete;
  const returnData = await removeCountry(id);

  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "País Eliminado"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();
}

//* CRUD CIUDADES

const getAllCitiesByCountryDB = async (arrCountriesId) =>{
  arrCountriesId.forEach( async (element) =>{
    const data = await getAllCitiesByCountry(element);
    console.log(data);
    createCities(data, element);
  });
}

const createCities = (data, country_id) =>{
  const  tableCities = document.querySelector(`#tableCities_${country_id}`);
  const tableBodyCities = tableCities.querySelector(`#bodyCities${country_id}`);

  data.forEach((registry) =>{
    tableBodyCities.innerHTML += `
    <tr>
      <td class="table__td">
        ${registry.name}
      </td>
      <td class="table__td table__td--icons">
      <i class="bi bi-pencil-square btnOpenModalEditCity" data-bs-toggle="modal" data-bs-target="#editCityModal" data-city-edit-id="${registry.id}" data-city-name="${registry.name}" ></i>  
      <i class="bi bi-trash btnOpenModalDeleteCity" data-bs-toggle="modal" data-bs-target="#deleteCityModal" data-city-delete-id="${registry.id}" ></i>
      </td>
    </tr>
    `;
  });

  const btnOpenModalEditCity = document.querySelectorAll('.btnOpenModalEditCity');
  const btnOpenModalDeleteCity = document.querySelectorAll('.btnOpenModalDeleteCity');

  btnOpenModalEditCity.forEach((btn) =>{
    btn.addEventListener('click', createModalEditCity);
  });
  btnOpenModalDeleteCity.forEach((btn) =>{
    btn.addEventListener('click', setIdCityDelete);
  });
  
}

const setIdCountryCreateCity = (event) =>{
  const id = event.target.dataset.countryId;
  btnCreateCity.setAttribute('data-country-id', id);
}

const createModalEditCity = (event) =>{
  console.log(event)
  const  name = event.target.dataset.cityName;
  const  id = event.target.dataset.cityEditId;
  

  formEditCity.innerHTML= `
    <label for="inputEditCity" class="form-label">Ciudad:</label>
    <input type="text" class="form-control inputsEditCity" id="inputEditCity" value="${name}" data-name-field="name">
  `;
  btnEditCity.setAttribute('data-city-edit-id', id);
  btnEditCity.setAttribute('data-city-edit-name', name);
}

const insertCity = async (event) =>{
  const country_id = event.target.dataset.countryId;
  const name = document.querySelector('#inputCreateCity').value;
  
  const returnData = await createCity({name, country_id});

  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Ciudad Creada"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();
}

const updateCity = async (event) =>{
  const id = event.target.dataset.cityEditId;
  const name = document.querySelector('#inputEditCity').value;
  const returnData = await editCity(id, {name});
  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Ciudad Actualizada"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();
}

const setIdCityDelete = (event) =>{
  const id = event.target.dataset.cityDeleteId;
  btnDeleteCity.setAttribute('data-city-delete', id);
}

const deleteCity = async (event) =>{
  console.log('Delete: ', event);
  const id = event.target.dataset.cityDelete;
  const returnData = await removeCity(id);

  //Alert
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Ciudad Eliminada"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getAllRegionsDB();
}


getAllRegionsDB();

btnCreateRegion.addEventListener('click', insertRegion);
btnEditRegion.addEventListener('click', updateRegion);
btnDeleteRegion.addEventListener('click', deleteRegion);

btnCreateCountry.addEventListener('click', insertCountry);
btnEditCountry.addEventListener('click', updateCountry);
btnDeleteCountry.addEventListener('click', deleteCountry);

btnCreateCity.addEventListener('click', insertCity);
btnEditCity.addEventListener('click', updateCity);
btnDeleteCity.addEventListener('click', deleteCity);

