import {authenticate} from './requests.js';

const message = document.querySelector('#message');
//Inputs
const inputEmail = document.querySelector('#inputEmail');
const inputPassword = document.querySelector('#inputPassword');
// Btn
const btnSend = document.querySelector('#btnSend');

const getDataForm = async () =>{
  message.classList.add('hidden');
  const token = await authenticate({email: inputEmail.value, password: inputPassword.value})

  if(token.code === 200){
    localStorage.setItem('token', token.token);
    localStorage.setItem('profile', token.r);
    location.href = "./contacts.html"
  }else{
    message.innerHTML = token.msg;
    message.classList.remove('hidden');
  }
}

btnSend.addEventListener('click', getDataForm);