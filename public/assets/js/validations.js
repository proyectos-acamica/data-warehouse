
const roleId = localStorage.getItem('profile');
const token = localStorage.getItem('token');


const validateNavUsers = () =>{
  const nav = document.querySelector('.nav');
  const type = nav.dataset.menu;
  let menu; 
  if(roleId === '1'){
    menu = `
      <ul class="nav__ul">
        <a href="contacts.html" class="nav__a"><li class="nav__li ${(type === 'Contactos' ? 'nav__li--active': '' )}">Contactos</li></a>
        <a href="companies.html" class="nav__a"><li class="nav__li ${(type === 'Compañias' ? 'nav__li--active': '' )}">Compañias</li></a>
        <a href="users.html" class="nav__a"><li class="nav__li ${(type === 'Usuarios' ? 'nav__li--active': '' )}">Usuarios</li></a>
        <a href="regions.html" class="nav__a"><li class="nav__li ${(type === 'Region' ? 'nav__li--active': '' )}">Región/Ciudad</li></a>
        <li class="nav__li" id="btnLogout"><i class="bi bi-box-arrow-right"></i> Salir</li>
      </ul>
      `;
    }else{
      menu = `
      <ul class="nav__ul">
        <a href="contacts.html" class="nav__a"><li class="nav__li ${(type === 'Contactos' ? 'nav__li--active': '' )}">Contactos</li></a>
        <a href="companies.html" class="nav__a"><li class="nav__li ${(type === 'Compañias' ? 'nav__li--active': '' )}">Compañias</li></a>
        <a href="regions.html" class="nav__a"><li class="nav__li ${(type === 'Region' ? 'nav__li--active': '' )}">Región/Ciudad</li></a>
        <li class="nav__li" id="btnLogout"><i class="bi bi-box-arrow-right"></i> Salir</li>
      </ul>
      `;
  }

  nav.innerHTML = menu;

  const btnLogout  = document.querySelector('#btnLogout');
  btnLogout.addEventListener('click',logout );
}

const validateLogin = () =>{
  if(!token && !roleId){
    location.href = "./index.html";
  }
}

const logout = () =>{
  localStorage.clear();
  location.href = "./index.html"
}

validateNavUsers();
validateLogin();