import {getAllContacts, deleteContact, getAllCities, getAllCompanies, createContact, editContact, deleteContactMultiple, createChannels,getAllChannels, getAllRelationsContact, updateChannels}  from './requests.js'
import {validateValueSelect}  from './utilities.js'

const formEditContacts = document.querySelector('#formEditContacts');
const tableBodyContacts = document.querySelector('#tableBodyContacts');
const btnDeleteContact = document.querySelector('#btnDeleteContact');
const btnDeleteAllContact = document.querySelector('#btnDeleteAllContact');
const btnEditContact = document.querySelector('#btnEditContact');
const btnCreateContact = document.querySelector('#btnCreateContact');

const inputSearch = document.querySelector('#inputSearch');
const btnSearch = document.querySelector('#btnSearch');

const btnAllDelete = document.querySelector('#btnAllDelete');
const badgeNotify = document.querySelector('#badgeNotify');

const toastTitle = document.querySelector('#toastTitle');
const toastTime = document.querySelector('#toastTime');
const toastMessage = document.querySelector('#toastMessage');

const toast = document.querySelector("#liveToast");
const newAlert = new bootstrap.Toast(toast);


let objDataContacts = {};
let arrCities = [];
let arrCompanies = [];
let arrInterests = [
  {id: 0, name:' 0%'},
  {id: 25, name:' 25%'},
  {id: 50, name:' 50%'},
  {id: 75, name:' 75%'},
  {id: 100, name:' 100%'},
];
let arrPreferences = [
  {id: 1, name:'Sin preferencia'},
  {id: 2, name:'Canal favorito'},
  {id: 3, name:'No molestar'},
];
let arrCheckbox = [];
let arrChannels = [];
let arrPivot = [];

const getDataDB = async () =>{
  objDataContacts = await getAllContacts();
  createTable(objDataContacts);
  await getAllCitiesDB();
  await getAllCompaniesDB();
  await getAllChannelsDB();
}

const getAllCitiesDB = async () =>{ 
  arrCities = await getAllCities();  
  setSelectCities(arrCities);
  return arrCities;
}

const getAllCompaniesDB = async () =>{ 
  arrCompanies = await getAllCompanies();  
  setSelectCompanies(arrCompanies);
  return arrCompanies;
}
const getAllChannelsDB = async () =>{ 
  arrChannels= await getAllChannels();  
  setSelectChannels(arrChannels);
  return arrChannels;
}

const getAllRelation = async (contact_id) =>{
  arrPivot= await getAllRelationsContact(contact_id);
  return arrPivot;
}

const createTable = (dataContacts) => {
  tableBodyContacts.innerHTML ="";
  dataContacts.forEach(registry =>{
    tableBodyContacts.innerHTML +=`
    <tr>
      <th scope="row" class="table__td"><input type="checkbox" class="checkContacts" data-contact-id ="${registry.id}"></th>
      <td class="table__td">
        <ul>
          <li class="table__li">
          ${registry.name} ${registry.lastName}
          </li>
          <li class="table__li--small">
          ${registry.email}
          </li>
        </ul>
      </td>
      <td class="table__td">
        <ul>
          <li class="table__li">
            ${registry.cityName}
          </li>
          <li class="table__li--small">
            ${registry.countryName}
          </li>
      </ul>
      </td>
      <td class="table__td">${registry.companyName}</td>
      <td class="table__td">${registry.position}</td>
      <td class="table__td">${registry.interest}%</td>
      <td class="table__td table__td--icons">
      <i class="bi bi-pencil-square btnOpenModalEdit" data-bs-toggle="modal" data-bs-target="#editContactModal" data-contact-edit-id="${registry.id}"></i>
      <i class="bi bi-trash btnOpenModalDelete" data-bs-toggle="modal" data-bs-target="#deleteContactModal" data-contact-delete-id="${registry.id}"></i>
      </td>
    </tr>
    `
  });
  const allBtnDelete = document.querySelectorAll('.btnOpenModalDelete');
  const allBtnEdit = document.querySelectorAll('.btnOpenModalEdit');
  const allChecks = document.querySelectorAll('.checkContacts');

  allBtnDelete.forEach(btn =>{
    btn.addEventListener('click', setIdContactDelete);
  });

  allBtnEdit.forEach(btn =>{
    btn.addEventListener('click', createBodyModalEdit);
  });

  allChecks.forEach( (check) =>{
    check.addEventListener('click', actionSelectCheck);
  });
}

const createBodyModalEdit = async (event) =>{
  const contact_id = event.target.dataset.contactEditId;
  const contact = objDataContacts.filter((contact) => contact.id === parseInt(contact_id));

  const relations = await getAllRelation(contact_id);
  

  formEditContacts.innerHTML = `
    <label for="inputEditContactName" class="form-label">Nombre:</label>
    <input type="text" class="form-control inputsEditContact" id="inputEditContactName" value="${contact[0].name}"data-name-field="name">
    <label for="inputEditContactLastName" class="form-label">Apellido:</label>
    <input type="text" class="form-control inputsEditContact" id="inputEditContactLastName" value="${contact[0].lastName}" data-name-field="lastName">
    <label for="inputEditContactPosition" class="form-label">Cargo:</label>
    <input type="text" class="form-control inputsEditContact" id="inputEditContactPosition" value="${contact[0].position}" data-name-field="position">
    <label for="inputEditContactEmail" class="form-label">Email:</label>
    <input type="email" class="form-control inputsEditContact" id="inputEditContactEmail" value="${contact[0].email}" data-name-field="email">
    <label for="inputEditContactInterest" class="form-label">Interés:</label>
    <select class="form-select inputsEditContact" aria-label="Default select example" id="inputEditContactInterest" data-name-field="interest">
      ${validateValueSelect(arrInterests, contact[0].interest )}      
    </select>    
    <label for="inputEditContactCompany" class="form-label">Compañia:</label>
    <select class="form-select inputsEditContact" aria-label="Default select example" id="inputEditContactCompany" data-name-field="company_id">
      ${validateValueSelect(await getAllCompaniesDB(), contact[0].companyId )}
    </select>
    <label for="inputEditContactCity" class="form-label">Ciudad:</label>
    <select class="form-select inputsEditContact" aria-label="Default select example" id="inputEditContactCity" data-name-field="city_id">
      ${validateValueSelect(await getAllCitiesDB(), contact[0].cityId )}
    </select>
      <div class="channels">
      <h2>Canales:</h2>
        <div class="channels__channel">
          <label for="inputCreateContactChannel" class="form-label">Canal de contacto:</label>
          <select class="form-select inputsEditContactChannel" disabled aria-label="Default select example" id="inputCreateContactChannel" data-name-field="channel_id">
                <option value="1" >${relations[0].channelName}</option>
          </select>     
          <label for="inputCreateCompanyAccount" class="form-label">Cuenta de usuario:</label>
          <input type="text" class="form-control inputsEditContactChannel" id="inputCreateCompanyAccount" data-name-field="account_user" value="${relations[0].account_user}">
          <label for="inputCreateContactPreferences" class="form-label">Peferencias:</label>
          <select class="form-select inputsEditContactChannel" aria-label="Default select example" id="inputEditContactInterest" data-name-field="preference">
            ${validateValueSelect(arrPreferences, relations[0].preference )}      
          </select>
        </div>
        <div class="channels__channel channels__channel--border">
          <label for="inputCreateContactChannel1" class="form-label">Canal de contacto:</label>
          <select class="form-select inputsEditContactChannel1" disabled aria-label="Default select example" id="inputCreateContactChannel1" data-name-field="channel_id">
                <option value="2" >${relations[1].channelName}</option>
          </select>
          <label for="inputCreateCompanyAccount1" class="form-label">Cuenta de usuario:</label>
          <input type="text" class="form-control inputsEditContactChannel1" id="inputCreateCompanyAccount1" data-name-field="account_user" value="${relations[1].account_user}">
          <label for="inputCreateContactPreferences1" class="form-label">Peferencias:</label>
          <select class="form-select inputsEditContactChannel1" aria-label="Default select example" id="inputCreateContactPreferences1" data-name-field="preference">
            ${validateValueSelect(arrPreferences, relations[1].preference )}   
          </select>
        </div>
      </div>
  `;

  btnEditContact.setAttribute('data-contact-edit', contact_id);
}


//* Acciones: 

const setSelectCities = (arrCities) => {
  const inputCreateContactCity = document.querySelector('#inputCreateContactCity');
  inputCreateContactCity.innerHTML = '';

  arrCities.forEach((city) =>{
    inputCreateContactCity.innerHTML += `
    <option value="${city.id}">${city.name}</option>
    `;
  });
}

const setSelectCompanies = (arrCompanies) => {
  const inputCreateContactCompany = document.querySelector('#inputCreateContactCompany');
  inputCreateContactCompany.innerHTML = '';

  arrCompanies.forEach((company) =>{
    inputCreateContactCompany.innerHTML += `
    <option value="${company.id}">${company.name}</option>
    `;
  });
}
const setSelectChannels = (arrChannels) => {
  const inputCreateContactChannel = document.querySelector('#inputCreateContactChannel');
  inputCreateContactChannel.innerHTML = '';

  arrChannels.forEach((channel) =>{
    inputCreateContactChannel.innerHTML += `
    <option value="${channel.id}">${channel.name}</option>
    `;
  });

  const inputCreateContactChannel1 = document.querySelector('#inputCreateContactChannel1');
  inputCreateContactChannel1.innerHTML = '';

  arrChannels.forEach((channel) =>{
    inputCreateContactChannel1.innerHTML += `
    <option value="${channel.id}">${channel.name}</option>
    `;
  });
}

const setDataCreateContacts  = async () =>{
  const inputsCreateContacts = document.querySelectorAll('.inputsCreateContact');
  const inputsCreateContactsChannel = document.querySelectorAll('.inputsCreateContactChannel');
  const inputsCreateContactsChannel1 = document.querySelectorAll('.inputsCreateContactChannel1');
  
  let objContactsData = {}
  let arrContactsDataChannel = []
  let objContactsDataChannel = {}

  inputsCreateContacts.forEach(element =>{
    objContactsData[element.dataset.nameField] = element.value
    element.value = ""
  });


  inputsCreateContactsChannel.forEach(element =>{
    objContactsDataChannel[element.dataset.nameField]= element.value;
    element.value = ""
  });

  arrContactsDataChannel.push(objContactsDataChannel);

  objContactsDataChannel = {}
  inputsCreateContactsChannel1.forEach(element =>{
    objContactsDataChannel[element.dataset.nameField]= element.value;
    element.value = ""
  });

  arrContactsDataChannel.push(objContactsDataChannel);

  const returnData = await createContact(objContactsData);
  // Impirmier en la alerta. 
  let insert

  if(returnData.length > 0){
    arrContactsDataChannel.forEach((channel) =>{
      console.log('Channels: ', channel);
      channel['contact_id'] = returnData[0];
    });
    console.log('arrContactsDataChannel: ', arrContactsDataChannel);
    console.log('→', inputsCreateContactsChannel);

    insert = await createChannels({channels : arrContactsDataChannel});
  }

  console.log('Respondio: ',insert);
  toastTitle.innerHTML = "Contacto Creado"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = insert;
  newAlert.show();

  console.log('Respondio: ',insert);
  getDataDB();
  //'El contacto, ha sido creado con éxito.'
}

const setDataUpdateContact = async (event) =>{
  const contact_id = event.target.dataset.contactEdit
  const inputsEditContact = document.querySelectorAll('.inputsEditContact');
  const inputsEditContactsChannel = document.querySelectorAll('.inputsEditContactChannel');
  const inputsEditContactsChannel1 = document.querySelectorAll('.inputsEditContactChannel1');
  
  let objContactData = {}
  let arrContactsDataChannel = []
  let objContactsDataChannel = {}

  inputsEditContact.forEach(element =>{
    objContactData[element.dataset.nameField] = element.value
  });

  inputsEditContactsChannel.forEach(element =>{
    objContactsDataChannel[element.dataset.nameField]= element.value;
    element.value = ""
  });

  arrContactsDataChannel.push(objContactsDataChannel);
  
  objContactsDataChannel = {}
  inputsEditContactsChannel1.forEach(element =>{
    objContactsDataChannel[element.dataset.nameField]= element.value;
    element.value = ""
  });

  arrContactsDataChannel.push(objContactsDataChannel);


  await editContact(contact_id, objContactData);

 
    arrContactsDataChannel.forEach((channel) =>{
      channel['contact_id'] = contact_id;
    });

    const insert = await updateChannels(contact_id,{channels : arrContactsDataChannel});
  
  // Impirmier en la alerta. 
  console.log('Respondio: ',insert);
  toastTitle.innerHTML = "Contacto Actualizado"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = insert;
  newAlert.show();
  getDataDB();
}

const setIdContactDelete = (event) =>{
  const contact_id = event.target.dataset.contactDeleteId;
  btnDeleteContact.setAttribute('data-contact-delete', contact_id);
}

const removeContact = async (event) =>{
  const contact_id = event.target.dataset.contactDelete;
  const data = await deleteContact(contact_id);
  // texto a enviar en el alert.
  console.log('Respondio: ',data);
  toastTitle.innerHTML = "Contacto Eliminado"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = data;
  newAlert.show();
  getDataDB();
} 


//* Delete All

const selectAllInput = () =>{
  const checkAll = document.querySelector('#checkAllContacts');  
  checkAll.addEventListener('change', function (){
    const allChecks = document.querySelectorAll('.checkContacts');
    arrCheckbox = [];
    if(this.checked){
      allChecks.forEach((check) =>{
        check.checked = 1
        console.log();
        arrCheckbox.push(check.dataset.contactId);
      });

      btnAllDelete.classList.remove('hidden'); 
    }else{
      allChecks.forEach((check) =>{
        check.checked = 0
        arrCheckbox = [];
      });
      btnAllDelete.classList.add('hidden');
    }
    badgeNotify.innerHTML = arrCheckbox.length;
  })
  
}

const actionSelectCheck = () =>{
  arrCheckbox = [];
  const allChecks = document.querySelectorAll('.checkContacts');
  allChecks.forEach((check)=>{

    if(check.checked == 1){
      arrCheckbox.push(check.dataset.contactId);
    }
  });
  if(arrCheckbox.length > 1){
    btnAllDelete.classList.remove('hidden');
    
  }else{
    btnAllDelete.classList.add('hidden');
  }
  badgeNotify.innerHTML = arrCheckbox.length;
}

const removeAllContacts = async () =>{
  const data = await deleteContactMultiple({contactIds: arrCheckbox});
  // texto a enviar en el alert.
  console.log('Respondio: ',data);
  toastTitle.innerHTML = "Contactos Eliminados"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = data;
  newAlert.show();
  
  getDataDB();
  btnAllDelete.classList.add('hidden');
  arrCheckbox = [];
}

const Search = () =>{
  const text = inputSearch.value.toLowerCase();
  const returnData = [];

  objDataContacts.forEach(element =>{
  console.log("Resultado ~ file: contacts.js ~ line 262 ~ Search ~ element", element);
    let name = element.name.toLowerCase();
    let lastName = element.lastName.toLowerCase();
    let email = element.email.toLowerCase();
    let position = element.position.toLowerCase();
    let company = element.companyName.toLowerCase();
    let city = element.cityName.toLowerCase();
    let country = element.countryName.toLowerCase();

    if(name.indexOf(text) !== -1 || lastName.indexOf(text)  !== -1 ||email.indexOf(text)  !== -1 || position.indexOf(text)  !== -1 || city.indexOf(text)  !== -1 || country.indexOf(text)  !== -1 ||company.indexOf(text)  !== -1 ){
      returnData.push(element);
    }

    createTable(returnData);
  });
}

getDataDB();
selectAllInput();

btnDeleteContact.addEventListener('click', removeContact);
btnDeleteAllContact.addEventListener('click', removeAllContacts);
btnCreateContact.addEventListener('click', setDataCreateContacts);
btnEditContact.addEventListener('click', setDataUpdateContact);
btnSearch.addEventListener('click', Search);


