
import {getAllUsers, createUser, editUser, deleteUser , getAllRoles}  from './requests.js'
import {validateValueSelect}  from './utilities.js'

const formEditUser = document.querySelector('#formEditUser');
const tableBodyUser = document.querySelector('#tableBodyUser');
const btnDeleteUser = document.querySelector('#btnDeleteUser');
const btnEditUser = document.querySelector('#btnEditUser');
const btnCreateUser = document.querySelector('#btnCreateUser');


const toastTitle = document.querySelector('#toastTitle');
const toastTime = document.querySelector('#toastTime');
const toastMessage = document.querySelector('#toastMessage');

const toast = document.querySelector("#liveToast");
const newAlert = new bootstrap.Toast(toast);


let objDataUsers = {};

const getDataDB = async () =>{
  objDataUsers = await getAllUsers();
  createTable(objDataUsers);
}

const getAllRolesDB = async () =>{ 
  return await getAllRoles();
}

const createTable = (dataUsers) => {
  tableBodyUser.innerHTML ="";
  dataUsers.forEach(registry =>{
    tableBodyUser.innerHTML +=`
    <tr>
      <td class="table__td">${registry.name}</td>
      <td class="table__td">${registry.lastName}</td>
      <td class="table__td">${registry.email}</td>
      <td class="table__td">${registry.profile}</td>
      <td class="table__td table__td--icons">
      <i class="bi bi-pencil-square btnOpenModalEdit" data-bs-toggle="modal" data-bs-target="#editUserModal" data-user-edit-id="${registry.id}"></i>
      <i class="bi bi-person-x btnOpenModalDelete" data-bs-toggle="modal" data-bs-target="#deleteUserModal" data-user-delete-id="${registry.id}"></i>
      </td>
    </tr>
    `
  });
  const allBtnDelete = document.querySelectorAll('.btnOpenModalDelete');
  const allBtnEdit = document.querySelectorAll('.btnOpenModalEdit');
  allBtnDelete.forEach(btn =>{
    btn.addEventListener('click', setIdUserDelete);
  });

  allBtnEdit.forEach(btn =>{
    btn.addEventListener('click', createBodyModalEdit);
  });
}

const createBodyModalEdit = async (event) =>{
  const user_id = event.target.dataset.userEditId;
  console.log(objDataUsers);
  const user = objDataUsers.filter((user) => user.id === parseInt(user_id));

  formEditUser.innerHTML = `
    <label for="inputEditUserName" class="form-label">Nombre:</label>
    <input type="text" class="form-control inputsEditUser" id="inputEditUserName" value="${user[0].name}" data-name-field="name">
    <label for="inputEditUserLastName" class="form-label">Apellido:</label>
    <input type="text" class="form-control inputsEditUser" id="inputEditUserLastName" value="${user[0].lastName}" data-name-field="lastName">
    <label for="inputEditUserEmail" class="form-label">Email:</label>
    <input type="email" class="form-control inputsEditUser" id="inputEditUserEmail" value="${user[0].email}" data-name-field="email">
    <label for="inputEditUserProfile" class="form-label">Perfil:</label>
    <select class="form-select inputsEditUser" aria-label="Default select example" data-name-field="role_id">
      ${validateValueSelect(await getAllRolesDB(), user[0].role_id )}
    </select>
    <label for="inputEditUserPassword" class="form-label">Contraseña:</label>
    <input type="password" class="form-control inputsEditUser" id="inputEditUserPassword" value="${user[0].password}" data-name-field="password">  
  `;

  btnEditUser.setAttribute('data-user-edit', user_id);
}


//* Acciones: 

const setDataCreateUser  = async () =>{
  const inputsCreateUser = document.querySelectorAll('.inputsCreateUser');
  let objUserData = {}
  inputsCreateUser.forEach(element =>{
    objUserData[element.dataset.nameField] = element.value
    element.value = ""
  });
  const returnData = await createUser(objUserData);
  
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Usuario Creado"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getDataDB();
}

const setDataUpdateUser = async (event) =>{
  const user_id = event.target.dataset.userEdit
  const inputsEditUser = document.querySelectorAll('.inputsEditUser');
  
  let objUserData = {}
  inputsEditUser.forEach(element =>{
    objUserData[element.dataset.nameField] = element.value
  });
  const returnData = await editUser(user_id, objUserData);
  // Impirmier en la alerta. 
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Usuario Actualizado"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  
  getDataDB();
}

const setIdUserDelete = (event) =>{
  const user_id = event.target.dataset.userDeleteId;
  btnDeleteUser.setAttribute('data-user-delete', user_id);
}

const removeUser = async (event) =>{
  const user_id = event.target.dataset.userDelete;
  const returnData = await deleteUser(user_id);
  // texto a enviar en el alert.
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Usuario Elimiando"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getDataDB();
} 


getDataDB();


btnDeleteUser.addEventListener('click', removeUser);
btnCreateUser.addEventListener('click', setDataCreateUser);
btnEditUser.addEventListener('click', setDataUpdateUser);