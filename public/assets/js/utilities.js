

const validateValueSelect = (dataDB, valueSelected) =>{ 
  let options = '';

  dataDB.forEach(registry =>{
    if(registry.id === parseInt(valueSelected)){
      options += `<option value="${registry.id}" selected>${registry.name}</option>`
    }else{
      options += `<option value="${registry.id}">${registry.name}</option>`
    }
  });

  return options;
}


export{
  validateValueSelect
}