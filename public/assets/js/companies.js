import {getAllCompanies, createCompany, editCompany, deleteCompany , getAllCities}  from './requests.js'
import {validateValueSelect}  from './utilities.js'

const formEditCompany = document.querySelector('#formEditCompany');
const tableBodyCompany = document.querySelector('#tableBodyCompany');
const btnDeleteCompany = document.querySelector('#btnDeleteCompany');
const btnEditCompany = document.querySelector('#btnEditCompany');
const btnCreateCompany = document.querySelector('#btnCreateCompany');

const toastTitle = document.querySelector('#toastTitle');
const toastTime = document.querySelector('#toastTime');
const toastMessage = document.querySelector('#toastMessage');

const toast = document.querySelector("#liveToast");
const newAlert = new bootstrap.Toast(toast);

let objDataCompanies= {};
let arrCities = [];

const getDataDB = async () =>{
  objDataCompanies = await getAllCompanies();
  createTable(objDataCompanies);  
  await getAllCitiesDB();
}

const getAllCitiesDB = async () =>{ 
  arrCities = await getAllCities();  
  setSelectCities(arrCities);
  return arrCities;
}

const createTable = (dataCompanies) => {
  tableBodyCompany.innerHTML ="";
  dataCompanies.forEach(registry =>{
    tableBodyCompany.innerHTML +=`
    <tr>
      <td class="table__td">${registry.name}</td>
      <td class="table__td">${registry.address}</td>
      <td class="table__td">${registry.email}</td>
      <td class="table__td">${registry.phone}</td>
      <td class="table__td">${registry.city}</td>
      <td class="table__td table__td--icons">
      <i class="bi bi-pencil-square btnOpenModalEdit" data-bs-toggle="modal" data-bs-target="#editCompanyModal" data-company-edit-id="${registry.id}"></i>
      <i class="bi bi-trash btnOpenModalDelete" data-bs-toggle="modal" data-bs-target="#deleteCompanyModal" data-company-delete-id="${registry.id}"></i>
      </td>
    </tr>
    `
  });

  const allBtnDelete = document.querySelectorAll('.btnOpenModalDelete');
  const allBtnEdit = document.querySelectorAll('.btnOpenModalEdit');
  allBtnDelete.forEach(btn =>{
    btn.addEventListener('click', setIdCompanyDelete);
  });

  allBtnEdit.forEach(btn =>{
    btn.addEventListener('click', createBodyModalEdit);
  });
}

const createBodyModalEdit = async (event) =>{
  const company_id = event.target.dataset.companyEditId;
  console.log(objDataCompanies);
  const company = objDataCompanies.filter((company) => company.id === parseInt(company_id));
  formEditCompany.innerHTML = `
    <label for="inputEditCompanyName" class="form-label">Nombre:</label>
    <input type="text" class="form-control inputsEditCompany" id="inputEditCompanyName" value="${company[0].name}" data-name-field="name">
    <label for="inputEditUserAddress" class="form-label">Apellido:</label>
    <input type="text" class="form-control inputsEditCompany" id="inputEditUserAddress" value="${company[0].address}" data-name-field="address">
    <label for="inputEditCompanyEmail" class="form-label">Email:</label>
    <input type="email" class="form-control inputsEditCompany" id="inputEditCompanyEmail" value="${company[0].email}" data-name-field="email">
    <label for="inputEditCompanyCity" class="form-label">Ciudad:</label>
    <select class="form-select inputsEditCompany" aria-label="Default select example" id="inputEditCompanyCity"  data-name-field="city_id">
      ${validateValueSelect(await getAllCitiesDB(), company[0].cityId )}
    </select>
  `;

  btnEditCompany.setAttribute('data-company-edit', company_id);
}

const setSelectCities = (arrCities) => {
  const inputCreateCompanyCity = document.querySelector('#inputCreateCompanyCity');
  inputCreateCompanyCity.innerHTML = '';

  arrCities.forEach((city) =>{
    inputCreateCompanyCity.innerHTML += `
    <option value="${city.id}">${city.name}</option>
    `;
  });
}


//* Acciones: 

const setDataCreateCompany  = async () =>{
  const inputsCreateCompany = document.querySelectorAll('.inputsCreateCompany');
  let objCompanyData = {}
  inputsCreateCompany.forEach(element =>{
    objCompanyData[element.dataset.nameField] = element.value
    element.value = ""
  });
  const returnData = await createCompany(objCompanyData);
  // Impirmier en la alerta. 
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Compañia Creada"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getDataDB();
}

const setDataUpdateCompany = async (event) =>{
  const company_id = event.target.dataset.companyEdit
  const inputsEditCompany = document.querySelectorAll('.inputsEditCompany');
  
  let objCompanyData = {}
  inputsEditCompany.forEach(element =>{
    objCompanyData[element.dataset.nameField] = element.value
  });
  const returnData = await editCompany(company_id, objCompanyData);
  // Impirmier en la alerta. 
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Compañia Actualizada"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getDataDB();
}

const setIdCompanyDelete = (event) =>{
  const company_id = event.target.dataset.companyDeleteId;
  btnDeleteCompany.setAttribute('data-company-delete', company_id);
}

const removeCompany = async (event) =>{
  const company_id = event.target.dataset.companyDelete;
  const returnData = await deleteCompany(company_id);
  // texto a enviar en el alert.
  console.log('Respondio: ',returnData);
  toastTitle.innerHTML = "Compañia Elimianda"
  toastTime.innerHTML = `hace ${new Date().getSeconds()} seg.` 
  toastMessage.innerHTML = returnData;
  newAlert.show();
  getDataDB();
} 

getDataDB();
btnDeleteCompany.addEventListener('click', removeCompany);
btnCreateCompany.addEventListener('click', setDataCreateCompany);
btnEditCompany.addEventListener('click', setDataUpdateCompany);