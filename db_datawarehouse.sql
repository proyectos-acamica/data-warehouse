-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-05-2021 a las 01:56:12
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_datawarehouse`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `channels`
--

CREATE TABLE `channels` (
  `channel_id` int(4) NOT NULL,
  `channel_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `channels`
--

INSERT INTO `channels` (`channel_id`, `channel_name`) VALUES
(1, 'Whatsapp'),
(2, 'Facebook');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `city_id` int(4) NOT NULL,
  `city_name` varchar(60) DEFAULT NULL,
  `city_country_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`city_id`, `city_name`, `city_country_id`) VALUES
(1, 'Bogotá', 3),
(2, 'Cali', 3),
(3, 'Buenos Aires', 1),
(4, 'Sao Paulo', 2),
(5, 'Barranquilla.', 3),
(9, 'Madrid', 6),
(12, 'Cusco', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE `companies` (
  `company_id` int(4) NOT NULL,
  `company_name` varchar(60) DEFAULT NULL,
  `company_address` varchar(50) DEFAULT NULL,
  `company_email` varchar(50) DEFAULT NULL,
  `company_phone` varchar(13) DEFAULT NULL,
  `company_city_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`company_id`, `company_name`, `company_address`, `company_email`, `company_phone`, `company_city_id`) VALUES
(1, 'Rappi', 'Calle 10 # 57-12.', 'rappi@gmail.com', '3001245678', 1),
(2, 'Caniaap', 'carrera 93c # 49f 74 sur', 'caniapp@gmail.com', '3143512068', 9),
(5, 'Acámica', 'calle 75- 41 sur', 'hola@acamica.com', '456741', 3),
(6, 'Globant', 'Calle 75 - 44t- norte', 'hola@globan.com', '456482132', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `contact_id` int(4) NOT NULL,
  `contact_name` varchar(45) DEFAULT NULL,
  `contact_lastName` varchar(45) DEFAULT NULL,
  `contact_position` varchar(60) DEFAULT NULL,
  `contact_email` varchar(60) DEFAULT NULL,
  `contact_interest` varchar(3) DEFAULT NULL,
  `contact_company_id` int(4) NOT NULL,
  `contact_city_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`contact_id`, `contact_name`, `contact_lastName`, `contact_position`, `contact_email`, `contact_interest`, `contact_company_id`, `contact_city_id`) VALUES
(45, 'test0', 'test', 'Developer', 'test', '50', 1, 2),
(46, 'Laura', 'Moreno', 'UX/UI', 'laura@email.com', '75', 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_has_channels`
--

CREATE TABLE `contacts_has_channels` (
  `contact_has_channel_channel_id` int(4) NOT NULL,
  `contact_has_channel_contact_id` int(4) NOT NULL,
  `contact_has_channel_preference` varchar(45) DEFAULT NULL,
  `contact_has_channel_account_user` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contacts_has_channels`
--

INSERT INTO `contacts_has_channels` (`contact_has_channel_channel_id`, `contact_has_channel_contact_id`, `contact_has_channel_preference`, `contact_has_channel_account_user`) VALUES
(1, 45, '3', '3143512069'),
(1, 46, '3', '3148754343'),
(2, 45, '2', 'facebook/david'),
(2, 46, '2', '/laura-rappi');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `country_id` int(4) NOT NULL,
  `country_name` varchar(60) DEFAULT NULL,
  `country_region_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_region_id`) VALUES
(1, 'Argentina', 1),
(2, 'Brasil', 1),
(3, 'Colombia', 1),
(4, 'Perú', 1),
(6, 'España', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regions`
--

CREATE TABLE `regions` (
  `region_id` int(4) NOT NULL,
  `region_name` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `regions`
--

INSERT INTO `regions` (`region_id`, `region_name`) VALUES
(1, 'Sudamérica'),
(2, 'Centroamérica'),
(3, 'Norteamérica'),
(4, 'Europa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `role_id` int(2) NOT NULL,
  `role_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Administrador'),
(2, 'Básico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(4) NOT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `user_lastName` varchar(45) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_password` varchar(45) DEFAULT NULL,
  `user_role_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_lastName`, `user_email`, `user_password`, `user_role_id`) VALUES
(1, 'David', 'Moreno', 'david@email.com', '123456', 1),
(6, 'María', 'Pachón', 'maría@gmail.com', '14514', 1),
(8, 'Carter', 'Moreno', 'carterM@gmail.com', '123456', 2),
(10, 'Lalo', 'Landia', 'lalo@gmail.com', '123456', 2),
(16, 'admin', 'admin', 'admin@admin.com', '123456', 1),
(17, 'basico', 'basico', 'basico@basico.com', '123456', 2),
(18, 'test', 'test', 'test@gmail.com', '123456', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`channel_id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`,`city_country_id`),
  ADD KEY `fk_cities_countries1` (`city_country_id`);

--
-- Indices de la tabla `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`company_id`,`company_city_id`),
  ADD KEY `fk_companies_cities1` (`company_city_id`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`,`contact_company_id`,`contact_city_id`),
  ADD KEY `fk_contacts_cities1` (`contact_city_id`);

--
-- Indices de la tabla `contacts_has_channels`
--
ALTER TABLE `contacts_has_channels`
  ADD PRIMARY KEY (`contact_has_channel_channel_id`,`contact_has_channel_contact_id`),
  ADD KEY `fk_channels_has_contacts_contacts1` (`contact_has_channel_contact_id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`,`country_region_id`),
  ADD KEY `fk_countries_regions1` (`country_region_id`);

--
-- Indices de la tabla `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`region_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`,`user_role_id`),
  ADD KEY `fk_users_roles` (`user_role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `channels`
--
ALTER TABLE `channels`
  MODIFY `channel_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `companies`
--
ALTER TABLE `companies`
  MODIFY `company_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `regions`
--
ALTER TABLE `regions`
  MODIFY `region_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `fk_cities_countries1` FOREIGN KEY (`city_country_id`) REFERENCES `countries` (`country_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `fk_contacts_cities1` FOREIGN KEY (`contact_city_id`) REFERENCES `cities` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contacts_has_channels`
--
ALTER TABLE `contacts_has_channels`
  ADD CONSTRAINT `fk_channels_has_contacts_channels1` FOREIGN KEY (`contact_has_channel_channel_id`) REFERENCES `channels` (`channel_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_channels_has_contacts_contacts1` FOREIGN KEY (`contact_has_channel_contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `countries`
--
ALTER TABLE `countries`
  ADD CONSTRAINT `fk_countries_regions1` FOREIGN KEY (`country_region_id`) REFERENCES `regions` (`region_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_roles` FOREIGN KEY (`user_role_id`) REFERENCES `roles` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
