const express = require('express');
const router = express.Router();

const {validateJWT} = require('../middlewares/validate-jwt.middleware');

const {index, store, edit, remove, getAllCitiesByCountryId} = require('../controllers/cities.controller');

router.get('/', validateJWT, index);
router.get('/country/:country_id', validateJWT, getAllCitiesByCountryId);
router.post('/', validateJWT, store);
router.put('/:city_id', validateJWT,edit);
router.delete('/:city_id', validateJWT, remove);

module.exports = router;
