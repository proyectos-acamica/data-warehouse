const express = require('express');
const router = express.Router();
const {validateJWT} = require('../middlewares/validate-jwt.middleware');
const {index, store, edit, remove} = require('../controllers/regions.controller');

router.get('/', validateJWT, index);
router.post('/', validateJWT, store);
router.put('/:region_id', validateJWT, edit);
router.delete('/:region_id', validateJWT, remove);

module.exports = router