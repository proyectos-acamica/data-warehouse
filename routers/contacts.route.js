const express = require('express');
const router = express.Router();
const {validateJWT} = require('../middlewares/validate-jwt.middleware');
const {index, store, edit, remove, removeMultiple, storeChannels, indexChannels, getAllRelation,editRelations} = require('../controllers/contacts.controller')

router.get('/', validateJWT, index);
router.get('/channels', validateJWT, indexChannels);
router.get('/channels/contact/:contact_id', validateJWT, getAllRelation);
router.post('/', validateJWT, store);
router.post('/channels', validateJWT, storeChannels);
router.put('/:contact_id', validateJWT, edit);
router.put('/channels/contact/:contact_id', validateJWT, editRelations);
router.delete('/:contact_id', validateJWT, remove);
router.delete('/', validateJWT, removeMultiple);

module.exports = router