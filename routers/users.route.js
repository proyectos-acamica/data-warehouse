const express = require('express');
const router = express.Router();

const {validateJWT} = require('../middlewares/validate-jwt.middleware');
const {isAdmin} = require('../middlewares/validate-role.middleware');
const {index, store, edit, remove, roles} = require('../controllers/users.controller');

router.get('/', validateJWT, isAdmin, index);
router.post('/', validateJWT, isAdmin, store);
router.put('/:user_id', validateJWT, isAdmin, edit);
router.delete('/:user_id', validateJWT, isAdmin, remove);
router.get('/roles', validateJWT, isAdmin, roles);

module.exports = router;

