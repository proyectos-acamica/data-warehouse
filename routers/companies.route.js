const express = require('express');
const router = express.Router();
const {validateJWT} = require('../middlewares/validate-jwt.middleware');
const {index, store, edit, remove} = require('../controllers/companies.controller')

router.get('/', validateJWT, index);
router.post('/', validateJWT, store);
router.put('/:company_id', validateJWT, edit);
router.delete('/:company_id', validateJWT, remove);


module.exports = router;