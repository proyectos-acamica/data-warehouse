const express = require('express');
const router = express.Router();
const {validateJWT} = require('../middlewares/validate-jwt.middleware');
const {index, store, edit, remove, getAllCountriesByRegionId} = require('../controllers/countries.controller');

router.get('/', validateJWT, index);
router.get('/region/:region_id', validateJWT, getAllCountriesByRegionId);
router.post('/', validateJWT, store);
router.put('/:country_id', validateJWT, edit);
router.delete('/:country_id', validateJWT, remove);

module.exports = router;