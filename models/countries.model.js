const sequelize = require('./connection');

const tableName = 'countries';

class CountriesModel {

  async getAllCountries (){
    const returnData = await sequelize.query(`SELECT country_id AS id, country_name AS name, country_region_id AS regionId FROM ${tableName}`,{
      type: sequelize.QueryTypes.SELECT
    });

    return returnData;
  }

  async createCountry (countryData){
    await sequelize.query(`INSERT INTO ${tableName} (${countryData.arrColumns}) VALUES ('${countryData.arrValues}')`,{
      type: sequelize.QueryTypes.INSERT
    })

    return 'El país, ha sido creado con éxito.';
  }

  async updateCountry (country_id, countryData) {
    await sequelize.query(`UPDATE ${tableName} SET ${countryData} WHERE country_id = ${country_id}`,{
      type: sequelize.QueryTypes.UPDATE
    });

    return 'El país, ha sido actualizado con éxito.'
  }

  async deleteCountry (country_id){
    await sequelize.query(`DELETE FROM ${tableName} WHERE country_id = ${country_id}`,{
      type: sequelize.QueryTypes.DELETE
    });

    return 'El país, ha sido eliminado con éxito.';
  }

  async getAllCountriesByRegions (region_id) {
    const returnData = await sequelize.query(`SELECT country_id AS id, country_name AS name FROM  ${tableName} WHERE country_region_id = ${region_id}`,{
      type: sequelize.QueryTypes.SELECT
    });

    return returnData;
  }

}

module.exports = new CountriesModel();