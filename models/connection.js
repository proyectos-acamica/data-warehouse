const Sequelize = require('sequelize');

const {DB_NAME, DB_HOST, DB_PASSWORD, DB_USER} = process.env

const sequelize = new Sequelize(DB_NAME || 'db_datawarehouse', DB_USER || 'root', DB_PASSWORD || '',
  {
    host: DB_HOST || 'localhost', 
    dialect: 'mysql'
  }
);

// Test connection

const testConnection = async () =>{
  try {
    await sequelize.authenticate();
    console.log('Conexion establecida!');
  } catch (error) {
    console.error('Conexion no establecidad! ', error);
  }
}

// testConnection();

module.exports = sequelize;
