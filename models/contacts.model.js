const sequelize = require('./connection');

const tableName = 'contacts';

class ContactsModel{

  async getAllContacts(){
    const returnData = await sequelize
    .query(`SELECT contact_id AS id, contact_name AS name, contact_lastName AS lastName, contact_position AS position, contact_email AS email, contact_interest AS interest, companies.company_id AS companyId, companies.company_name AS companyName, cities.city_id AS cityId, cities.city_name AS cityName, countries.country_name AS countryName FROM ${tableName} INNER JOIN companies ON companies.company_id = contact_company_id INNER JOIN cities ON  cities.city_id = contact_city_id INNER JOIN countries ON  countries.country_id = cities.city_country_id`,{
      type: sequelize.QueryTypes.SELECT
    })

    return returnData;
  }

  async createContact(contactData){
    const returnData = await sequelize.query(`INSERT INTO ${tableName} (${contactData.arrColumns}) VALUES ('${contactData.arrValues}')`,{
      type: sequelize.QueryTypes.INSERT
    })
    return returnData;
  }

  async updateContact(contact_id, contactData){
    await sequelize.query(`UPDATE ${tableName} SET ${contactData} WHERE contact_id = ${contact_id}`,{
      type: sequelize.QueryTypes.UPDATE
    })
    return 'El contacto, ha sido actualizado con éxito.';
  }

  async deleteContact(contact_id){
    await sequelize.query(`DELETE FROM ${tableName} WHERE contact_id = ${contact_id}`,{
      type: sequelize.QueryTypes.DELETE
    })
    return 'El contacto, ha sido eliminado con éxito.';
  }
  
  async deleteMultipleContacts (contactData) {
    await sequelize.query(`DELETE FROM ${tableName} WHERE contact_id IN (${contactData})`,{
      type: sequelize.QueryTypes.DELETE
    })
    return 'los contactos, han sido eliminados con éxito.';
  }

  async createChannels (contactData){
    const tableName = 'contacts_has_channels';
    await sequelize
    .query(`INSERT INTO ${tableName} (${contactData.arrColumns}) VALUES ('${contactData.arrValues}') `,
    {
      type: sequelize.QueryTypes.INSERT
    })

    return 'El contacto, ha sido creado con éxito.';
  }

  async getAllChannels (){
    const tableName = 'channels'
    const returnData = await sequelize
    .query(`SELECT channel_id AS id, channel_name AS name FROM ${tableName}`,{
      type: sequelize.QueryTypes.SELECT
    })

    return returnData;
  }
  
  async getAllChannelsByContact (contact_id){
    const tableName = 'contacts_has_channels'
    const returnData = await sequelize
    .query(`SELECT channels.channel_id AS channelId, channels.channel_name AS channelName, contact_has_channel_preference AS preference, contact_has_channel_account_user AS account_user FROM ${tableName} INNER JOIN channels ON channels.channel_id = contact_has_channel_channel_id WHERE contact_has_channel_contact_id = ${contact_id}`,{
      type: sequelize.QueryTypes.SELECT
    })

    return returnData;
  }

  async updatRelations (contact_id, channel_id, contactData) {
    await sequelize.query(`UPDATE contacts_has_channels SET ${contactData} WHERE contact_has_channel_contact_id = ${contact_id} AND contact_has_channel_channel_id = ${channel_id}`,{
      type: sequelize.QueryTypes.UPDATE
    })
    return 'El contacto, ha sido actualizado con éxito.';  
  }
}

module.exports = new ContactsModel();