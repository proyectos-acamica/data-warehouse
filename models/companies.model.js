const sequelize = require('./connection');

const tableName = 'companies'

class CompaniesModel {

  async getAllCompanies (){
    const returnData = await sequelize
    .query(`SELECT company_id AS id, company_name AS name, company_address AS address, company_email AS email, company_phone AS phone, cities.city_id AS cityId, cities.city_name AS city FROM ${tableName} INNER JOIN cities ON cities.city_id = company_city_id`,{
      type: sequelize.QueryTypes.SELECT
    });

    return returnData; 
  }

  async createCompany (companyData){
    await sequelize.query(`INSERT INTO ${tableName} (${companyData.arrColumns}) VALUES ('${companyData.arrValues}')`,{
      type: sequelize.QueryTypes.INSERT
    });

    return 'La compañia, ha sido creada con éxito.'
  }

  async updateCompany (company_id, companyData) {
    await sequelize.query(`UPDATE ${tableName} SET ${companyData} WHERE company_id = ${company_id}`,{
      type: sequelize.QueryTypes.UPDATE
    });

    return 'La compañia, ha sido actualizada con éxito.'
  }

  async deleteCompany (company_id) {
    await sequelize.query(`DELETE FROM ${tableName} WHERE company_id = ${company_id}`);
    return 'La compañia, ha sido eliminada con éxito.'
  }

}

module.exports = new CompaniesModel();