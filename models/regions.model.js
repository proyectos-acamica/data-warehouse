const sequelize = require('./connection');

const tableName = 'regions'

class RegionsModel {

  async getAllRegions () {
    const returnData = await sequelize
    .query(`SELECT region_id AS id, region_name AS name FROM ${tableName}`, {
      type: sequelize.QueryTypes.SELECT
    });
    return returnData;
  }

  async createRegion (regionData) {
    await sequelize.query(`INSERT INTO ${tableName} (${regionData.arrColumns}) VALUES ('${regionData.arrValues}')`,{
      type: sequelize.QueryTypes.INSERT
    });
    return 'La región, ha sido creada con éxito.'
  }

  async updateRegion (region_id, regionData) {
    await sequelize
      .query(`UPDATE ${tableName} SET ${regionData} WHERE region_id = ${region_id}`, {
        type: sequelize.QueryTypes.UPDATE
      });
      return 'La región, ha sido actualizada con éxito.'
  }

  async deleteRegion (region_id) {
    await sequelize.query(`DELETE FROM ${tableName} WHERE region_id = ${region_id}`,{
      type: sequelize.QueryTypes.DELETE
    });

    return 'La región, ha sido eliminada con éxito.'
  }
}

module.exports = new RegionsModel();
