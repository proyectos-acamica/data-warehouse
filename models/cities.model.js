const sequelize = require('./connection');

const tableName = 'cities'

class CitiesModel{

  async getAllCities (){
    const returnData = await sequelize
    .query(`SELECT city_id AS id, city_name AS name FROM ${tableName}`,{
      type: sequelize.QueryTypes.SELECT
    });
    return returnData;
  }

  async createCity (cityData) {
    await sequelize.query(`INSERT INTO ${tableName} (${cityData.arrColumns}) VALUES ('${cityData.arrValues}')`, {
      type: sequelize.QueryTypes.INSERT
    });

    return 'La ciudad, ha sido creada con éxito.'
  }

  async updateCity (city_id, cityData){
    await sequelize.query(`UPDATE ${tableName} SET ${cityData} WHERE city_id = ${city_id}`,{
      type: sequelize.QueryTypes.UPDATE
    })

    return 'La ciudad, ha sido actualizada con éxito.'
  }

  async deleteCity (city_id){
    await sequelize.query(`DELETE FROM ${tableName} WHERE city_id = ${city_id}`,{
      type: sequelize.QueryTypes.DELETE
    });

    return 'La ciudad, ha sido eliminada con éxito.'
  }

  async getAllCitiesByCountry (country_id){
    const returnData = await sequelize
    .query(`SELECT city_id AS id, city_name AS name FROM ${tableName} WHERE city_country_id = ${country_id}`,
    {
      type: sequelize.QueryTypes.SELECT
    });
    return returnData
  }
}

module.exports = new CitiesModel();