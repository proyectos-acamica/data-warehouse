const sequelize = require('./connection');
const tableName = 'users';

class UsersModel {
  
  async getUserById(id){
    
    const returnData = await sequelize
      .query(`SELECT user_name AS name, user_lastName AS lastName, user_email AS email, roles.role_name AS profile FROM ${tableName} INNER JOIN roles ON roles.role_id = users.roles_role_id WHERE user_id = ${id}`, 
        {
          type: sequelize.QueryTypes.SELECT
        }
      );
  
      return returnData;
  }

  async getUserByEmail(email){

    const returnData = await sequelize
    .query(`SELECT user_id AS id, user_role_id AS roleId, user_password AS password FROM ${tableName} WHERE user_email = "${email}"`,
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    return returnData
  }

  async getAllUsers(){

    const returnData = await sequelize
    .query(`SELECT user_id AS id, user_name AS name, user_lastName AS lastName, user_email AS email, user_password AS password, roles.role_id AS role_id, roles.role_name AS profile FROM ${tableName} INNER JOIN roles ON  roles.role_id = users.user_role_id`, 
      {
        type: sequelize.QueryTypes.SELECT
      }
    );
    return returnData;
  }

  async createUser(userData){
    await sequelize
    .query(`INSERT INTO ${tableName} (${userData.arrColumns}) VALUES ('${userData.arrValues}') `,{
      type: sequelize.QueryTypes.INSERT
    });
    return 'El usuario, ha sido creado con éxito.'
  }

  async updateUser(user_id, userData){
    await sequelize
      .query(`UPDATE ${tableName} SET ${userData} WHERE user_id = ${user_id}`, {
        type: sequelize.QueryTypes.UPDATE
      });
      return 'El usuario, ha sido actualziado con éxito.'
  }

  async deleteUser(user_id){

    await sequelize
      .query(`DELETE FROM ${tableName} WHERE user_id = ${user_id}`,{
        type: sequelize.QueryTypes.DELETE
      });
      return 'El usuario, ha sido eliminado con éxito.'
  }

  async getAllRoles (){
    const returnData = await sequelize
    .query(`SELECT role_id AS id, role_name AS name FROM roles`,{
      type: sequelize.QueryTypes.SELECT
    });

    return returnData;
  }
}

module.exports = new UsersModel();