const {getAllRegions, createRegion, updateRegion, deleteRegion} = require('../models/regions.model');
const {formatFields} = require('../helpers/utilities.helper');

const tablePrefix = 'region';

class RegionsController {

  async index (req, res){
    try {
      const returnData = await getAllRegions();
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al obtener las regiones.');
      res.json('No se ha podido obtener las regiones.').status(500)
    }
  }

  async store (req, res){
    try {
      const returnData = await createRegion(formatFields(tablePrefix, 'insert', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al crear la región: ', error);
      res.json('La región, no ha sido creada con éxito.').status(500);
    }
  }

  async edit (req, res){
    const {region_id} = req.params;
    try {
      const returnData = await updateRegion(region_id, formatFields(tablePrefix, 'update', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al editar la región: ', error);
      res.json('La región, no ha sido actualizada con éxito.').status(500);
    }
  }

  async remove (req, res){
    const {region_id} = req.params;
    try {
      const returnData = await deleteRegion(region_id);
      res.json(returnData).status(200)
    } catch (error) {
      console.log('Error al eliminar la región', error);
      res.json('La región, no ha sido eliminada con éxito.');
    }
  }

}

module.exports = new RegionsController();


