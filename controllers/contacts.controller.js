const {getAllContacts, createContact, updateContact, deleteContact, deleteMultipleContacts, createChannels, getAllChannels, getAllChannelsByContact, updatRelations} = require('../models/contacts.model')
const {formatFields} = require('../helpers/utilities.helper');

const tablePrefix = 'contact';

class ContactsController{

  async index (req, res){
    try {
      const returnData = await getAllContacts();
      res.json(returnData).status(200)
    } catch (error) {
      console.log('Error al obtener los contactos: ', error);
      res.json('No se ha podido listar los contactos.').status(500);
    }
  }

  async store (req, res){
    try {
      const returnData = await createContact(formatFields(tablePrefix, 'insert', req.body));
      res.json(returnData).status(200)
    } catch (error) {
      console.log('Error al crear el contacto: ', error);
      res.json('El contacto, no ha sido creado con éxito.')
    }
  }

  async edit (req, res){
    const {contact_id} = req.params;
    try {
      const returnData = await updateContact(contact_id,  formatFields(tablePrefix, 'update', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al actualizar el contacto: ', error);
      res.json('El contacto, no ha sido actualizado con éxito.')
    }
  }

  async remove (req, res){
    const {contact_id} = req.params;
    try {
      const returnData = await deleteContact(contact_id);
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al eliminar el contacto: ', error);
      res.json('El contacto, no ha sido eliminado con éxito.').status(500);
    }
  }

  async removeMultiple (req, res){
    const {contactIds} = req.body;
    const contactData = contactIds.toString();

    try {
      const returnData = await deleteMultipleContacts(contactData);
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al eliminar los contactos: ', error);
      res.json('Los contactos, no han sido eliminados con éxito.').status(500);
    }
  }

  async storeChannels (req, res){
    try {
      const {channels} = req.body;

      channels.forEach(async (channel) =>{
        await createChannels(formatFields("contact_has_channel", 'insert', channel));
      });
      res.json('El contacto, ha sido creado con éxito.').status(200)
    } catch (error) {
      console.log('Error al crear el contacto: ', error);
      res.json('El contacto, no ha sido creado con éxito.')
    }
  }

  async indexChannels (req, res){    
    try {
      const returnData = await getAllChannels();
      res.json(returnData).status(200)
    } catch (error) {
      console.log('Error al obtener los canales: ', error);
      res.json('No se ha podido listar los canales.').status(500);
    }
  }

  async getAllRelation (req, res){
    const {contact_id} = req.params;
    try {
      const returnData = await getAllChannelsByContact(contact_id);
      res.json(returnData).status(200)
    } catch (error) {
      console.log('Error al obtener los canales: ', error);
      res.json('No se ha podido listar los canales.').status(500);
    }
    
  }

  async editRelations (req, res) {
    try {
      const {contact_id} = req.params;
      const {channels} = req.body;

      channels.forEach(async (channel) =>{
        await updatRelations(contact_id,channel.channel_id,formatFields("contact_has_channel", 'update', channel));
      });
      res.json('El contacto, ha sido actualizado con éxito.').status(200)
    } catch (error) {
      console.log('Error al crear el contacto: ', error);
      res.json('El contacto, no ha sido actualizado con éxito.')
    }
    
  }
}

module.exports = new ContactsController();