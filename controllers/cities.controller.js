const {getAllCities, createCity, updateCity, deleteCity, getAllCitiesByCountry} = require('../models/cities.model');
const {formatFields} = require('../helpers/utilities.helper');

const tablePrefix = 'city';

class CitiesController {

  async index (req, res){    
    try {
      const returnData = await getAllCities();
      res.json(returnData).status(200);  
    } catch (error) {
      console.log('Error al obtener la ciudades: ', error);
      res.json('No se pudo obtener las ciudades.').status(500);
    }
  }

  async store (req, res){
    try {
      const returnData = await createCity(formatFields(tablePrefix, 'insert', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al crear la ciudad: ', error);
      res.json('La ciudad, no ha sido creada.').status(500);
    }
  }

  async edit (req, res){
    const {city_id} = req.params;
    try {
      const returnData = await updateCity(city_id,  formatFields(tablePrefix, 'update', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al actualizar la ciudad: ', error);
      res.json('La ciudad, no ha sido actualizada con éxito.').status(500);
    }
  }

  async remove (req, res){
    const {city_id} = req.params;
    try {
      const returnData = await deleteCity(city_id);
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al eliminar la ciudad: ', error);
      res.json('La ciudad, no ha sido eliminada con éxito.').status(500);
    }
  }

  async getAllCitiesByCountryId (req, res){
    const {country_id} = req.params;
    try {
      const returnData = await getAllCitiesByCountry(country_id);
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al consultar las ciudades: ', error);
      res.json('No se han podido listar las ciudades.').status(500);
    }
  }
}

module.exports = new CitiesController();