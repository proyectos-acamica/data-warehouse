const {getUserByEmail} = require('../models/users.model');
const {getToken} = require('../helpers/generateJWT.helper');

class AuthController{

  async login (req, res){
    const {email, password} = req.body;

    try {
      const validateEmail = await getUserByEmail(email);
      if(validateEmail.length === 0 || validateEmail[0].password != password){
        return res.json({msg:"Usuario o contraseña inconrrectos.", code:400}).status(400);
      }

      const token = await getToken(validateEmail[0].id, validateEmail[0].roleId);

      res.json({token, code:200, r:validateEmail[0].roleId}).status(200)
    } catch (error) {
      console.log('Error al consultar el usuario: ', error);
      res.json({msg: 'Error al validar el token.', code:500}).status(500);
    }
  }
}


module.exports = new AuthController();




