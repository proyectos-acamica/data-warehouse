const {getAllCompanies, createCompany, updateCompany, deleteCompany} = require('../models/companies.model');
const {formatFields} = require('../helpers/utilities.helper');

const tablePrefix = 'company'

class CompaniesController{

  async index (req, res){
    try {
      const returnData = await getAllCompanies();
      res.json(returnData).status(200)
    } catch (error) {
      console.log('Error al obtener compañias: ', error);
      res.json('No se han obtenido las compañias.').status(500);
    }
  }

  async store (req, res){
    try {
      const returnData = await createCompany(formatFields(tablePrefix, 'insert', req.body));
      res.json(returnData).status(200)
    } catch (error) {
      console.log('Error al crear la compañia: ', error);
      res.json('La compañia, no ha sido creada con éxito.').status(500)
    }
  }

  async edit (req, res){
    const {company_id} = req.params;

    try {
      const returnData = await updateCompany(company_id,  formatFields(tablePrefix, 'update', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al actualizar la compañia: ', error);
      res.json('La compañia, no ha sido actualizada con éxito.');
    }
  }

  async remove (req, res){
    
    const {company_id} = req.params;

    try {
      const returnData = await deleteCompany(company_id);
      res.json(returnData).status(200)
    } catch (error) {
      console.log('Error al eliminar la compañia: ', error);
      res.json('La compañia, no ha sido eliminada con éxito.').status(500)
    }
  }

}

module.exports = new  CompaniesController();