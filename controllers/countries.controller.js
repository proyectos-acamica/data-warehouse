const {getAllCountries, createCountry, updateCountry, deleteCountry, getAllCountriesByRegions} = require('../models/countries.model');
const {formatFields} = require('../helpers/utilities.helper');

const tablePrefix = 'country';

class CountriesController {

  async index (req, res){    
    try {
      const returnData = await getAllCountries();
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al obtener los paises: ', error);
      res.json('No se ha podido consultar el listado de los paises.').status(500);
    }
  }

  async store (req, res){
    try {
      const returnData = await createCountry(formatFields(tablePrefix, 'insert', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al crear el país: ', error);
      res.json('El país, no ha sido creado con éxito.').status(500);
    }
  }

  async edit (req, res){
    const {country_id} = req.params;

    try {
      const returnData = await updateCountry(country_id, formatFields(tablePrefix,'update', req.body));
      res.json(returnData).status(200);
    } catch (error) { 
      console.log('Error al actualizar el país: ', error);
      res.json('El país, no ha sido actualizado con éxito.');
    }
  }  

  async remove (req, res){
    const {country_id} = req.params;
    try {
      const returnData = await deleteCountry(country_id);
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al eliminar el país: ', error);
      res.json('El país, no ha sido eliminado con éxito.');
    }
  }

  async getAllCountriesByRegionId (req, res){
    const {region_id} = req.params;
    console.log('qqqqq', region_id);
    try {
      const returnData = await getAllCountriesByRegions(region_id);
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Erro al obtener los países por región');
      res.json('Los paises no pudieron ser listado').status(500);
    }
  }
}

module.exports = new CountriesController();