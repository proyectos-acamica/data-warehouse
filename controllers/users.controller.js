const {getAllUsers,createUser, updateUser, deleteUser, getAllRoles} =  require('../models/users.model');
const {formatFields} = require('../helpers/utilities.helper');

const tablePrefix = 'user'

class UsersController{

  async index (req, res){    
    try {
      let returnData = await getAllUsers();
      res.json(returnData).status(200)
    } catch (error) {
      console.log(error);
      res.json('No es posible listar los usuarios.').status(500);
    }
  }

  async store (req, res){
    try {
      const returnData = await createUser(formatFields(tablePrefix, 'insert', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al crear el usuario.', error);
      res.json('El usuario, no ha sido creado con éxito.').status(500)
    }
  }

  async edit (req, res){
    const {user_id} = req.params;
    try {
      const returnData = await updateUser(user_id,  formatFields(tablePrefix, 'update', req.body));
      res.json(returnData).status(200);
    } catch (error) {
      res.json('El usuario, no ha sido actualizado con éxito.').status(500);
    }
  }

  async remove (req, res){
    const {user_id} = req.params;
    try {
      const returnData = await deleteUser(user_id);
      res.json(returnData).status(200);
    } catch (error) {
      res.json('El usuario, no ha sido eliminado con éxito.').status(500);
    }
  }

  async roles (req, res){
    try {
      const returnData = await getAllRoles();
      res.json(returnData).status(200);
    } catch (error) {
      console.log('Error al obtener roles: ', error);
      res.json('No se han podido listar los roles.').status(500);
    }
  }

}


module.exports = new UsersController();