const express = require('express');
const morgan = require('morgan');

require('dotenv').config();

const app = express();

const {HOST_PORT} = process.env;


app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(morgan('dev'));

app.use(express.static('public'));
app.use('/auth', require('./routers/auth.route'));
app.use('/users', require('./routers/users.route'));
app.use('/regions', require('./routers/regions.route'));
app.use('/countries', require('./routers/countries.route'));
app.use('/cities', require('./routers/cities.route'));
app.use('/companies', require('./routers/companies.route'));
app.use('/contacts', require('./routers/contacts.route'));



app.listen(HOST_PORT, () =>{
  console.log(`El servidor esta corriendo en el puerto: ${HOST_PORT}`);
})